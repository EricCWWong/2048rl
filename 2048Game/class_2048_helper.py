import numpy as np


def merge_row(row):
    '''
        This function merges a given row to the right.
        It is same as swiping right in the game 2048.

        Example:
        >>> # our input...
        >>> row = [2,0,2,0]
        >>> new_row = merge_row(row)
        >>> new_row
        >>> # our output...
        >>> [0,0,0,4]
    '''
    no_zero_row = []
    for el in row:
        if el != 0:
            no_zero_row.append(el)

    new_row = []
    
    if len(no_zero_row) == 2:
        if no_zero_row[0] == no_zero_row[1]:
            new_row.append(no_zero_row[0] + no_zero_row[1])
        else:
            new_row = no_zero_row
    elif len(no_zero_row) == 3:
        if no_zero_row[0] == no_zero_row[1]:
            new_row.append(no_zero_row[0] + no_zero_row[1])
            new_row.append(no_zero_row[2])
        elif no_zero_row[1] == no_zero_row[2]:
            new_row.append(no_zero_row[0])
            new_row.append(no_zero_row[1] + no_zero_row[2])
        else:
            new_row = no_zero_row
    elif len(no_zero_row) == 4:
        if no_zero_row[0] == no_zero_row[1]:
            new_row.append(no_zero_row[0] + no_zero_row[1])
            if no_zero_row[2] == no_zero_row[3]:
                new_row.append(no_zero_row[2] + no_zero_row[3])
            else:
                new_row.append(no_zero_row[2])
                new_row.append(no_zero_row[3])
        
        elif no_zero_row[1] == no_zero_row[2]:
            new_row.append(no_zero_row[0])
            new_row.append(no_zero_row[1] + no_zero_row[2])
            new_row.append(no_zero_row[3])
        elif no_zero_row[2] == no_zero_row[3]:
            new_row.append(no_zero_row[0])
            new_row.append(no_zero_row[1])
            new_row.append(no_zero_row[2] + no_zero_row[3])
        else:
            new_row = no_zero_row
    else:
        new_row = no_zero_row

    if len(new_row) != 4:
        no_empty = 4 - len(new_row)
        for _ in range(no_empty):
            new_row = [0] + new_row

    return new_row

def merge(grid):
    '''
        This uses the merge row function above and calculate
        the new grid after merging.

        Example:
        >>> # our input...
        >>> grid = [[2,0,2,0]
                    [0,0,4,0]
                    [4,4,0,0]
                    [2,8,0,2]]
        >>> new_grid = merge(grid)
        >>> new_grid
        >>> # our output...
        >>> [[0,0,0,4]
             [0,0,0,4]
             [0,0,0,8]
             [0,2,8,2]]
    '''
    
    tiles = grid
    new_grid = []
    for row in tiles:
        new_tiles = merge_row(row)
        new_grid.append(new_tiles)
    return np.array(new_grid)

def neighbour_difference(grid, axis=None):
    '''
        This calculates the differences between neighbouring tiles.

        Example:
        >>> # our input...
        >>> grid = [[2,0,2,0]
                    [0,0,4,0]
                    [4,4,0,0]
                    [2,8,0,2]]
        >>> neig_diff = neighbour_difference(grid)
        >>> neig_diff
        >>> [2,-2, 2, 0,-4, 4, 0, 4, 0, -6, 8, -2]
    '''
    
    hor_diff = []
    vert_diff = []
    for row in grid:
        for i in range(3):
            hor_diff.append(row[i] - row[i + 1])
    
    for row in np.transpose(grid):
        for i in range(3):
            vert_diff.append(row[i] - row[i + 1])
    
    if axis == 0:
        return np.array(hor_diff)
    elif axis == 1:
        return np.array(vert_diff)
    else:
        return np.insert(hor_diff, len(hor_diff), vert_diff)


def reward_merge(old, new, max_tile=2048):
    '''
        This reward scheme gives reward by the number of new merged tiles.
    '''
    # initialise reward.
    reward = 0

    # flatten the vector.
    old = old.flatten()
    new = new.flatten()

    # create a dictonary that will store the values of the tiles.
    old_dict = {}
    old_dict[0] = 0
    new_dict = {}
    new_dict[0] = 0

    for i in range(int(np.log2(max_tile))):
        old_dict[int(2 ** (i + 1))] = 0
        new_dict[int(2 ** (i + 1))] = 0

    # counting the number of each values.
    for el in old:
        old_dict[int(el)] = old_dict[int(el)] + 1

    for el in new:
        new_dict[int(el)] += 1

    # now we check the difference between grids.
    diff = {}

    for i in range(int(np.log2(max_tile))):
        difference = new_dict[int(2 ** (i + 1))] - old_dict[int(2 ** (i + 1))]
        if difference > 0:
            diff[int(2 ** (i + 1))] = difference
        else:
            diff[int(2 ** (i + 1))] = 0

    # now we calculate the rewards, we only care about 4 or above.
    for key in diff:
        if key > 2:
            reward = reward + key * diff[key]

    return reward
